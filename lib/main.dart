import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:ti_yich_test/data/userprofile_dao.dart';
import 'package:ti_yich_test/screens/dispatcher_page.dart';
import 'data/feed_dao.dart';
import 'firebase_options.dart';
import 'package:provider/provider.dart';
import 'package:device_preview/device_preview.dart';

import '../data/user_dao.dart';
import '../data/message_dao.dart';

import 'package:ti_yich_test/screens/login_page.dart';
import 'home.dart';
import 'screens/chat_page.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(
    DevicePreview(
      enabled: !kReleaseMode,
      builder: (context) => MyApp(), // Wrap your app
    ),
  );
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  FirebaseAuth auth = FirebaseAuth.instance;

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        // TODO: Add ChangeNotifierProvider<UserDao> here
        ChangeNotifierProvider<UserDao>(
          lazy: false,
          create: (_) => UserDao(),
        ),

        Provider<MessageDao>(
          lazy: false,
          create: (_) => MessageDao(),
        ),

        Provider<FeedDao>(
          lazy: false,
          create: (_) => FeedDao(),
        ),
        Provider<UserProfileDao>(
          lazy: false,
          create: (_) => UserProfileDao(),
        ),
      ],
      child: MaterialApp(
        useInheritedMediaQuery: true,
        locale: DevicePreview.locale(context),
        builder: DevicePreview.appBuilder,
        debugShowCheckedModeBanner: false,
        title: 'Ti Yich Test',
        home: DispatcherPage(),
      ),
    );
  }
}
