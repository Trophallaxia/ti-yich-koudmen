import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ti_yich_test/models/misc_functions.dart';
import 'package:ti_yich_test/models/user_profile_data.dart';
import '../models/users_skills.dart';
import '../models/users_needs.dart';
import '../models/users_offers.dart';
import 'dart:convert';

import 'package:ti_yich_test/const.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          UserProfileInfos(),
          SizedBox(height: 20),
          UserNeeds(profileUID: FirebaseAuth.instance.currentUser!.uid),
          SizedBox(height: 20),
          UserSkills(profileUID: FirebaseAuth.instance.currentUser!.uid),
          SizedBox(height: 20),
          UserOffers(profileUID: FirebaseAuth.instance.currentUser!.uid),
        ],
      ),
    );
  }
}

class UserProfileInfos extends StatefulWidget {
  UserProfileInfos({Key? key}) : super(key: key);

  @override
  State<UserProfileInfos> createState() => _UserProfileInfosState();
}

class _UserProfileInfosState extends State<UserProfileInfos> {
  var _user = FirebaseAuth.instance.currentUser;
  var _ref = FirebaseFirestore.instance.collection('UserProfiles');
  @override
  Widget build(BuildContext context) {
    var profiledoc = _ref.doc(_user?.uid).get();
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              width: double.infinity,
              color: darkGreenCol,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Mon Profil',
                    style: bold_white_title,
                  ),
                  // Row(
                  //   children: [
                  //     IconButton(onPressed: () {}, icon: Icon(Icons.edit)),
                  //     IconButton(
                  //         onPressed: () {},
                  //         icon: Icon(
                  //           Icons.settings,
                  //           color: Colors.blueGrey,
                  //         )),
                  //   ],
                  // )
                ],
              )),
          FutureBuilder(
              future: _ref.doc(_user?.uid).get(),
              builder: (context,
                  AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>>
                      snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.hasData) {
                  Map<String, dynamic> data =
                      snapshot.data!.data() as Map<String, dynamic>;
                  return Column(
                    children: [
                      CircleAvatar(
                          backgroundColor:
                              PseudoAvatarColor(data['displayname']),
                          radius: 50,
                          child: Text(
                            data['displayname'][0] + data['displayname'][1],
                            style: TextStyle(color: Colors.white, fontSize: 50),
                          )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // TODO : Name and firstname
                          // Text(data['firstname'] as String),
                          // SizedBox(width: 5),
                          // Text(data['name'] as String),
                          Text(data['displayname']),
                        ],
                      ),
                      Text(data['location'] as String),
                      // TODO : Add point system
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: [
                      //     Text('Expérimentateur' as String),
                      //     SizedBox(width: 10),
                      //     Text('${user_data['points']} points'),
                      //   ],
                      // )
                    ],
                  );
                }

                return Text("loading");
              })
        ],
      ),
    );
  }
}

// class UserOffers extends StatelessWidget {
//   const UserOffers({Key? key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       height: 500,
//       width: MediaQuery.of(context).size.width,
//       color: Colors.blue[300],
//       child: Center(child: Text('Ici les offres User')),
//     );
//   }
// }
