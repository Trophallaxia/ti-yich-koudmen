import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:provider/provider.dart';
import 'package:ti_yich_test/data/user_dao.dart';
import 'package:ti_yich_test/home.dart';
import 'package:ti_yich_test/screens/login_page.dart';
import 'package:ti_yich_test/screens/onboarding_page.dart';
import '../firebase_options.dart';
import 'chat_page.dart';

class DispatcherPage extends StatelessWidget {
  DispatcherPage({Key? key}) : super(key: key);
  User? user = FirebaseAuth.instance.currentUser;
  @override
  Widget build(BuildContext context) {
    return Consumer<UserDao>(
      // 2
      builder: (context, userDao, child) {
        // 3
        if (userDao.isLoggedIn()) {
          return const OnboardingScreen();
        } else {
          return const LoginPage();
        }
      },
    );
  }
}
