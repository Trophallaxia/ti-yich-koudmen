import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_signin_button/button_builder.dart';
import 'package:ti_yich_test/const.dart';
import 'package:ti_yich_test/screens/dispatcher_page.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;

/// Entrypoint example for registering via Email/Password.
class RegisterPage extends StatefulWidget {
  /// The page title.
  final String title = 'Registration';

  @override
  State<StatefulWidget> createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _emailController = TextEditingController();
  final TextEditingController _nameController = TextEditingController();
  final TextEditingController _displaynameController = TextEditingController();
  final TextEditingController _firstnameController = TextEditingController();
  final TextEditingController _locationController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final TextEditingController _passwordVerifController =
      TextEditingController();

  bool? _success;
  String _userEmail = '';

  @override
  Widget build(BuildContext context) {
    return InteractiveViewer(
      maxScale: 4,
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('Inscription'),
          backgroundColor: darkGreenCol,
        ),
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomRight,
                    colors: [Color(0xFF74E2DB), Color(0xFFE6D8FC)])),
            child: Form(
              key: _formKey,
              child: Padding(
                padding: const EdgeInsets.all(16),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    TextFormField(
                      controller: _emailController,
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: 'Email',
                        prefixIcon: Icon(Icons.email_outlined),
                        fillColor: darkGreenCol,
                        filled: true,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                      validator: (String? value) {
                        if (value!.isEmpty) {
                          return 'Ce champ ne peut être vide';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    // TextFormField(
                    //   controller: _nameController,
                    //   decoration: InputDecoration(
                    //     labelText: 'Nom',
                    //     prefixIcon: Icon(Icons.person),
                    //     fillColor: darkGreenCol,
                    //     filled: true,
                    //     border: OutlineInputBorder(
                    //         borderRadius: BorderRadius.circular(10)),
                    //   ),
                    //   validator: (String? value) {
                    //     if (value!.isEmpty) {
                    //       return 'Ce champ ne peut être vide';
                    //     }
                    //     return null;
                    //   },
                    // ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                    // TextFormField(
                    //   controller: _firstnameController,
                    //   decoration: InputDecoration(
                    //     labelText: 'Prenom',
                    //     prefixIcon: Icon(Icons.person),
                    //     fillColor: darkGreenCol,
                    //     filled: true,
                    //     border: OutlineInputBorder(
                    //         borderRadius: BorderRadius.circular(10)),
                    //   ),
                    //   validator: (String? value) {
                    //     if (value!.isEmpty) {
                    //       return 'Ce champ ne peut être vide';
                    //     }
                    //     return null;
                    //   },
                    // ),
                    // SizedBox(
                    //   height: 10,
                    // ),
                    TextFormField(
                      controller: _displaynameController,
                      decoration: InputDecoration(
                        labelText: 'Pseudo',
                        prefixIcon: Icon(Icons.person),
                        fillColor: darkGreenCol,
                        filled: true,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                      validator: (String? value) {
                        if (value!.isEmpty) {
                          return 'Ce champ ne peut être vide';
                        }
                        if (value.length < 2) {
                          return "Le pseudo doit faire au moins deux caractères";
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      controller: _locationController,
                      decoration: InputDecoration(
                        labelText: 'Commune',
                        prefixIcon: Icon(Icons.location_on),
                        fillColor: darkGreenCol,
                        filled: true,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                      validator: (String? value) {
                        if (value!.isEmpty) {
                          return 'Ce champ ne peut être vide';
                        }
                        return null;
                      },
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      controller: _passwordController,
                      decoration: InputDecoration(
                        labelText: 'Mot de passe',
                        prefixIcon: Icon(Icons.password),
                        fillColor: darkGreenCol,
                        filled: true,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'Ce champ ne peut être vide';
                        }
                        return null;
                      },
                      obscureText: true,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    TextFormField(
                      controller: _passwordVerifController,
                      decoration: InputDecoration(
                        labelText: 'Confirmez le mot de passe',
                        prefixIcon: Icon(Icons.password),
                        fillColor: darkGreenCol,
                        filled: true,
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(10)),
                      ),
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'Ce champ ne peut être vide';
                        } else if (value != _passwordController.value.text) {
                          return 'Les mots de passes ne correspondent pas';
                        }
                        return null;
                      },
                      obscureText: true,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: ElevatedButton(
                          onPressed: () async {
                            FocusScope.of(context).unfocus();
                            if (_formKey.currentState!.validate()) {
                              await _register();
                            }
                          },
                          child: Text('Inscription'),
                          style: ButtonStyle(
                              backgroundColor:
                                  MaterialStateProperty.all(purpleCol))),
                    ),
                    // Container(
                    //   padding: const EdgeInsets.symmetric(vertical: 16),
                    //   alignment: Alignment.center,
                    //   child: SignInButtonBuilder(
                    //     icon: Icons.person_add,
                    //     backgroundColor: Colors.blueGrey,
                    //     onPressed: () async {
                    //       if (_formKey.currentState!.validate()) {
                    //         await _register();
                    //       }
                    //     },
                    //     text: 'Register',
                    //   ),
                    // ),
                    Container(
                      alignment: Alignment.center,
                      child: Text(
                        _success == null
                            ? ''
                            : (_success!
                                ? 'Inscription réussi pour $_userEmail'
                                : 'Inscription échouée'),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    // Clean up the controller when the Widget is disposed
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  // Example code for registration.
  Future<void> _register() async {
    //
    try {
      (await _auth.createUserWithEmailAndPassword(
        email: _emailController.text,
        password: _passwordController.text,
      ));
    } on FirebaseAuthException catch (e) {
      _success = false;
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text(e.message ?? 'Erreur inconnue')));
      setState(() {});
      return;
    } catch (e) {
      _success = false;
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text(e.toString())));
      setState(() {});
      return;
    }
    final User? user = _auth.currentUser;
    var _ref =
        FirebaseFirestore.instance.collection('UserProfiles').doc(user?.uid);
    if (user != null) {
      await user.updateDisplayName(_displaynameController.text);
      await _ref.set({
        'uid': user.uid,
        'name': _nameController.text,
        'firstname': _firstnameController.text,
        'displayname': _displaynameController.text,
        'location': _locationController.text
      }, SetOptions(merge: true));
      Navigator.of(context)
          .push(MaterialPageRoute(builder: (_) => DispatcherPage()));
      _userEmail = user.email ?? '';
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Inscription réussi pour $_userEmail')));
      setState(() {
        _success = true;
      });
    } else {
      _success = false;
    }
  }
}
