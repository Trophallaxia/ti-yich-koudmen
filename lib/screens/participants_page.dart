import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/src/provider.dart';
import 'package:ti_yich_test/const.dart';
import 'package:ti_yich_test/data/user_dao.dart';
import 'package:ti_yich_test/models/misc_functions.dart';

import 'dispatcher_page.dart';

class ParticipanstPage extends StatelessWidget {
  final List<dynamic> idlist;
  const ParticipanstPage({Key? key, required this.idlist}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InteractiveViewer(
      maxScale: 4,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: darkGreenCol,
          leading: Padding(
            padding: const EdgeInsets.all(5.0),
            child: GestureDetector(
              onTap: () {
                // Navigator.of(context)
                //     .push(MaterialPageRoute(builder: (_) => AddPhotos()));
              },
              child: CircleAvatar(
                backgroundImage:
                    AssetImage('assets/images/logo-karisko-moyen-300x236.png'),
              ),
            ),
          ),
          actions: [
            Column(
              children: [
                Flexible(
                  child: IconButton(
                      tooltip: 'Déconnexion',
                      onPressed: () {
                        var userDao = context.read<UserDao>();
                        userDao.logout();
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (context) => DispatcherPage()),
                            (Route<dynamic> route) => false);
                      },
                      icon: Icon(Icons.logout)),
                ),
                Text('Déconnexion  '),
              ],
            )
          ],
          title: Text(
              FirebaseAuth.instance.currentUser?.displayName ?? 'Anonymous'),
        ),
        body: Container(
          decoration: backgroundGradient,
          child: ListView.builder(
              itemCount: idlist.length,
              itemBuilder: (context, index) {
                return FutureBuilder(
                    future: FirebaseFirestore.instance
                        .collection('UserProfiles')
                        .doc(idlist[index])
                        .get(),
                    builder: (BuildContext context,
                        AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>>
                            snapshot) {
                      if (snapshot.hasError) {
                        return Text('Une erreur est survenue');
                      }
                      if (!snapshot.hasData) {
                        return Text('Une erreur est survenue');
                      }
                      {
                        var data =
                            snapshot.data!.data() as Map<String, dynamic>;
                        String pseudo = data['displayname'];
                        return ListTile(
                          leading: CircleAvatar(
                            backgroundColor: PseudoAvatarColor(pseudo),
                            child: Text(pseudo[0] + pseudo[1]),
                          ),
                          title: Text(data['displayname']),
                        );
                      }
                    });
              }),
        ),
      ),
    );
  }
}
