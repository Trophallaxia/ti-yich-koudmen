import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:provider/provider.dart';
import 'package:ti_yich_test/data/feed.dart';
import 'package:ti_yich_test/data/feed_dao.dart';
import 'dart:math';

import 'package:ti_yich_test/models/misc_functions.dart';

import '../models/feed_widget.dart';

// var feed = ['un', 'deux', 'trois', 'quatre', 'cinq'];

var feed = [
  {
    'username': 'Amy Whi',
    'title': "Faire un jardin créole",
    'slot': "Lun-Ven",
    'details': "Je n'ai pas la main verte :("
  },
  {
    'username': 'Ben David',
    'title': "Remplir ma fiche d'impôts",
    'slot': "Sam-Dim",
    'details': "Je n'y comprends rien"
  }
];

class NewsFeedPage extends StatefulWidget {
  const NewsFeedPage({Key? key}) : super(key: key);

  @override
  State<NewsFeedPage> createState() => _NewsFeedPageState();
}

class _NewsFeedPageState extends State<NewsFeedPage> {
  final _feedStream = FirebaseFirestore.instance.collection('Feed').snapshots();
  @override
  Widget build(BuildContext context) {
    final feedDao = Provider.of<FeedDao>(context, listen: false);
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            ListTile(
              title: Text('Autour de vous 📍'),
              trailing: IconButton(
                icon: Icon(Icons.refresh),
                onPressed: () => setState(() {}),
              ),
            ),
            //
            //
            // getfeedlist
            _getFeedList(feedDao),
            //
            //
            // StreamBuilder<QuerySnapshot>(
            //   stream: _feedStream,
            //   builder:
            //       (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            //     if (snapshot.hasError) {
            //       return Text('Something went wrong');
            //     }

            //     if (snapshot.connectionState == ConnectionState.waiting) {
            //       return Text("Loading");
            //     }

            //     return ListView(
            //       padding: EdgeInsets.all(0),
            //       physics: NeverScrollableScrollPhysics(),
            //       shrinkWrap: true,
            //       children: sortList(snapshot.data!.docs)
            //           .map((DocumentSnapshot document) {
            //         Map<String, dynamic> data =
            //             document.data()! as Map<String, dynamic>;

            //         DocumentReference<Map<String, dynamic>> _userRef =
            //             FirebaseFirestore.instance
            //                 .collection('UserProfiles')
            //                 .doc(data['uid']);
            //         return FutureBuilder<DocumentSnapshot>(
            //           future: _userRef.get(),
            //           builder: (context, snapshot) {
            //             if (snapshot.connectionState == ConnectionState.done) {
            //               Map<String, dynamic> userdata =
            //                   snapshot.data!.data() as Map<String, dynamic>;
            //               String firstname = userdata['firstname'] ?? ' ';
            //               String name = userdata['name'] ?? ' ';
            //               return ExpansionTile(
            //                 leading: CircleAvatar(
            //                   child: Text(firstname[0] + name[0]),
            //                   backgroundColor: AvatarColor(firstname, name),
            //                 ),
            //                 title: Text(feedTitle(firstname + ' ' + name,
            //                     data['type'], data['content']['title'])),
            //                 children: [
            //                   ListTile(
            //                     leading: Text('Créneau :'),
            //                     title: Text(data['content']['slot']),
            //                   ),
            //                   ListTile(
            //                     leading: Text('Détails'),
            //                     title: Text(data['content']['details']),
            //                   ),
            //                   ListTile(
            //                       // title: Text(_userRef.toString()),
            //                       )
            //                 ],
            //               );
            //             }

            //             return Text("loading");
            //           },
            //         );
            //       }).toList(),
            //     );
            //   },
            // ),
            //
            // end of getfeedlist
            //
            // ListView.builder(
            //     physics: NeverScrollableScrollPhysics(),
            //     padding: EdgeInsets.all(0),
            //     shrinkWrap: true,
            //     itemCount: feed.length,
            //     itemBuilder: (BuildContext context, int index) {
            //       return ExpansionTile(
            //         leading: CircleAvatar(),
            //         title: Text(
            //             "${feed[index]['username']!} aimerait ${feed[index]['title']!}"),
            //         children: [
            //           ListTile(
            //             leading: Text('Créneau :'),
            //             title: Text(feed[index]['slot']!),
            //           ),
            //           ListTile(
            //             leading: Text('Détails :'),
            //             title: Text(feed[index]['details']!),
            //           )
            //         ],
            //       );
            //     })
          ],
        ),
      ),
    );
  }

  Widget _getFeedList(FeedDao feedDao) {
    return StreamBuilder<QuerySnapshot>(
      // 2
      stream: feedDao.getFeedStream(),
      // 3
      builder: (context, snapshot) {
        // 4
        if (!snapshot.hasData) {
          return const Center(child: LinearProgressIndicator());
        }

        // 5
        return _buildList(context, snapshot.data!.docs);
      },
    );
  }

  Widget _buildList(BuildContext context, List<DocumentSnapshot>? snapshot) {
    var list =
        snapshot!.map((data) => _buildFeedListItem(context, data)).toList();
    // ignore: lines_longer_than_80_chars
    list.sort((a, b) => a.date.compareTo(b.date));

    // ScrollController _controller = ScrollController();

    // SchedulerBinding.instance?.addPostFrameCallback((_) {
    //   _controller.jumpTo(_controller.position.maxScrollExtent);
    // });
    // 1

    return ListView(
      reverse: false,
      shrinkWrap: true,

      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.only(top: 20.0),
      // 2
      children: list,
    );
  }

  FeedWidget _buildFeedListItem(
      BuildContext context, DocumentSnapshot snapshot) {
    // 1
    final feedItem = FeedItem.fromSnapshot(snapshot);
    // 2

    return FeedWidget(feedItem.content, feedItem.date, feedItem.type,
        feedItem.uid, feedItem.reference, feedItem.participants);
  }
}

String feedTitle(String name, String type, String title) {
  String typephrase;
  switch (type) {
    case "need":
      {
        typephrase = 'aimerait :';
      }

      break;
    case "service_offer":
      {
        typephrase = 'propose :';
      }

      break;

    case "gears_offer":
      {
        typephrase = 'propose :';
      }

      break;
    default:
      {
        return '';
      }
  }
  ;

  return '$name $typephrase $title';
}

List<QueryDocumentSnapshot<Object?>> sortList(
    List<QueryDocumentSnapshot<Object?>> thelist) {
  thelist.sort((A, B) {
    Map<String, dynamic> a = A.data()! as Map<String, dynamic>;
    Map<String, dynamic> b = B.data()! as Map<String, dynamic>;
    Timestamp c = a['date'];
    Timestamp d = a['date'];

    return c.compareTo(d);
  });

  return thelist;
}
