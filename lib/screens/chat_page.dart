import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

import 'package:provider/provider.dart';
import '../data/user_dao.dart';
import '../data/message.dart';
import '../data/message_dao.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import '../models/message_widget.dart';

import '../data/user_dao.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          color: Colors.red[800],
          child: Text(
            'Attention les messages ici sont visibles par toutes les personnes inscrites sur cette application',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
          ),
        ),
        Expanded(child: MessageList()),
      ],
    );
  }
}

class MessageList extends StatefulWidget {
  const MessageList({Key? key}) : super(key: key);

  @override
  MessageListState createState() => MessageListState();
}

class MessageListState extends State<MessageList> {
  final TextEditingController _messageController = TextEditingController();
  final ScrollController _scrollController = ScrollController();
  // TODO: Add Email String
  String? email;
  String? displayName;

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance!.addPostFrameCallback((_) => _scrollToBottom());

    // TODO: Add MessageDao
    final messageDao = Provider.of<MessageDao>(context, listen: false);

    // TODO: Add UserDao
    final userDao = Provider.of<UserDao>(context, listen: false);
    email = userDao.email();
    displayName = userDao.displayName();

    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        children: [
          // TODO: Add Message DAO to _getMessageList
          _getMessageList(messageDao),

          // Row(
          //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //   children: [
          //     SizedBox(
          //       width: 10,
          //     ),
          //     Row(
          //       children: [
          //         Text('Défiler vers le bas'),
          //         IconButton(
          //             onPressed: _scrollToBottom,
          //             icon: Icon(Icons.arrow_drop_down_circle)),
          //       ],
          //     ),
          //   ],
          // ),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12.0),
                  child: TextField(
                    keyboardType: TextInputType.text,
                    controller: _messageController,
                    onSubmitted: (input) {
                      // TODO: Add Message DAO 1
                      _sendMessage(messageDao);
                    },
                    decoration:
                        const InputDecoration(hintText: 'Entrer un message'),
                  ),
                ),
              ),
              IconButton(
                  icon: Icon(_canSendMessage()
                      ? CupertinoIcons.arrow_right_circle_fill
                      : CupertinoIcons.arrow_right_circle),
                  onPressed: () {
                    // TODO: Add Message DAO 2
                    _sendMessage(messageDao);
                  })
            ],
          ),
        ],
      ),
    );
  }

  // TODO: Replace _sendMessage
  void _sendMessage(MessageDao messageDao) {
    if (_canSendMessage()) {
      final message = Message(
        text: _messageController.text,
        date: DateTime.now(),

        // TODO: add email
        email: displayName,
      );
      print('UID = ${FirebaseAuth.instance.currentUser?.uid.toString()}');
      messageDao.saveMessage(message);
      _messageController.clear();
      setState(() {});
    }
  }

  // TODO: Replace _getMessageList
  Widget _getMessageList(MessageDao messageDao) {
    return Expanded(
      // 1
      child: StreamBuilder<QuerySnapshot>(
        // 2
        stream: messageDao.getMessageStream(),
        // 3
        builder: (context, snapshot) {
          // 4
          if (!snapshot.hasData)
            return const Center(child: LinearProgressIndicator());

          // 5
          return _buildList(context, snapshot.data!.docs);
        },
      ),
    );
  }

  // TODO: Add _buildList
  Widget _buildList(BuildContext context, List<DocumentSnapshot>? snapshot) {
    var list = snapshot!.map((data) => _buildListItem(context, data)).toList();
    // ignore: lines_longer_than_80_chars
    list.sort((a, b) => a.date.compareTo(b.date));

    ScrollController _controller = ScrollController();

    void _scrollToEnd() async {
      if (_controller.hasClients) {
        _controller.animateTo(_controller.position.maxScrollExtent,
            duration: Duration(milliseconds: 200), curve: Curves.easeInOut);
      }
    }

    SchedulerBinding.instance?.addPostFrameCallback((_) => _scrollToEnd());

    return ListView(
      reverse: false,
      controller: _controller,
      physics: const BouncingScrollPhysics(),
      padding: const EdgeInsets.only(top: 20.0),
      // 2
      children: list,
    );
  }

  // TODO: Add _buildListItem
  MessageWidget _buildListItem(
      BuildContext context, DocumentSnapshot snapshot) {
    // 1
    final message = Message.fromSnapshot(snapshot);
    // 2
    return MessageWidget(message.text, message.date, message.email);
  }

  bool _canSendMessage() => _messageController.text.trim().length > 0;

  void _scrollToBottom() async {
    if (_scrollController.hasClients) {
      _scrollController.animateTo(_scrollController.position.maxScrollExtent,
          duration: Duration(milliseconds: 200), curve: Curves.easeInOut);
    }
  }
}
