// import 'package:flutter/material.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase_core/firebase_core.dart';

// var _auth = FirebaseAuth.instance;
// var _user = _auth.currentUser;
// var _ref = FirebaseFirestore.instance.collection('DBTest').doc(_user?.uid);
// var _stream = _ref.get();
// var _skill = 0;

// class DBTest extends StatefulWidget {
//   const DBTest({Key? key}) : super(key: key);

//   @override
//   _DBTestState createState() => _DBTestState();
// }

// class _DBTestState extends State<DBTest> {
//   @override
//   Widget build(BuildContext context) {
//     return Container(
//       child: Column(
//         children: [
//           Container(
//             width: 500,
//             height: 200,
//             color: Colors.amber[100],
//             child: StreamBuilder(
//               stream: _ref.snapshots(),
//               builder: (BuildContext context,
//                   AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>>
//                       snapshot) {
//                 if (snapshot.hasError) {
//                   return Text('Something went wrong');
//                 }

//                 if (snapshot.connectionState == ConnectionState.waiting ||
//                     !snapshot.hasData) {
//                   return Text("Loading");
//                 }

//                 if (snapshot.data == null) {
//                   return Text("No Data yet");
//                 }

//                 // Map<String, dynamic> data =
//                 //     snapshot.data?.data() as Map<String, dynamic> ?? {};

//                 Map<String, dynamic> data = snapshot.data?.data() != null
//                     ? snapshot.data?.data() as Map<String, dynamic>
//                     : {};
//                 List skillList = data['skills'] ?? [];

//                 return ListView.builder(
//                     itemCount: skillList.length,
//                     itemBuilder: (context, index) {
//                       return ListTile(
//                         title: Text(skillList[index]),
//                       );
//                     });
//               },
//             ),
//           ),
//           ElevatedButton(
//               onPressed: () {
//                 //Ajouter skill a la DB
//                 _ref.set({
//                   'skills': FieldValue.arrayUnion([_skill.toString()])
//                 }, SetOptions(merge: true)).then(
//                     (value) => print('Button pressed'));
//                 _skill++;
//               },
//               child: Text('PRESS ME'))
//         ],
//       ),
//     );
//   }
// }
