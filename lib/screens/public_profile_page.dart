import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:ti_yich_test/data/message.dart';
import 'package:ti_yich_test/data/user_dao.dart';
import 'package:ti_yich_test/data/userprofile.dart';
import 'package:ti_yich_test/data/userprofile_dao.dart';
import 'package:ti_yich_test/models/misc_functions.dart';
import 'package:ti_yich_test/models/users_needs.dart';
import 'package:ti_yich_test/models/users_offers.dart';
import 'package:ti_yich_test/models/users_skills.dart';

import '../const.dart';
import 'add_photos.dart';
import 'dispatcher_page.dart';

var _auth = FirebaseAuth.instance;

UserProfile fakeUserProfile = UserProfile(
    uid: "000000",
    nom: "Fake",
    prenom: "Data",
    location: "ici",
    skills: ["un", "deux"]);

class PublicProfilePage extends StatefulWidget {
  final String profileUID;
  const PublicProfilePage({Key? key, required this.profileUID})
      : super(key: key);

  @override
  State<PublicProfilePage> createState() => _PublicProfilePageState();
}

class _PublicProfilePageState extends State<PublicProfilePage> {
  @override
  Widget build(BuildContext context) {
    final userProfileDao = Provider.of<UserProfileDao>(context, listen: false);

    // Build the ProfilePage with the object
    return InteractiveViewer(
      maxScale: 4,
      child: Scaffold(
          appBar: AppBar(
            backgroundColor: darkGreenCol,
            leading: Padding(
              padding: const EdgeInsets.all(5.0),
              child: GestureDetector(
                  onTap: () {
                    Navigator.of(context).pop();
                  },
                  child: Icon(Icons.home)),
            ),
            actions: [
              Column(
                children: [
                  Flexible(
                    child: IconButton(
                        tooltip: 'Déconnexion',
                        onPressed: () {
                          var userDao = context.read<UserDao>();
                          userDao.logout();
                          Navigator.of(context).pushAndRemoveUntil(
                              MaterialPageRoute(
                                  builder: (context) => DispatcherPage()),
                              (Route<dynamic> route) => false);
                        },
                        icon: Icon(Icons.logout)),
                  ),
                  Text('Déconnexion  '),
                ],
              )
            ],
            title: Text(_auth.currentUser?.displayName ?? 'Anonymous'),
          ),
          body: SafeArea(
            child: Container(
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomRight,
                      colors: [Color(0xFF74E2DB), Color(0xFFE6D8FC)])),
              child: Center(
                  child:
                      _buildProfilefromDB(userProfileDao, widget.profileUID)),
            ),
          )),
    );
  }

// Get the snapshot
  // Changer CurrentUser.uid to custom.uid
  Widget _buildProfilefromDB(UserProfileDao dao, String profileUID) {
    return FutureBuilder<DocumentSnapshot>(
        future: dao.getUserProfile(profileUID),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: LinearProgressIndicator(),
            );
          }
          return __buildProfilefromSnapshot(context, snapshot.data!);
        });
  }

// Convert Snapshot into UserProfile object
  Widget __buildProfilefromSnapshot(
      BuildContext context, DocumentSnapshot snapshot) {
    var userProfileObject = UserProfile.fromSnapshot(snapshot);
    return FillProfilePage(userProfile: userProfileObject);
  }
}

// Build the ProfilePage with the object
class FillProfilePage extends StatefulWidget {
  final UserProfile userProfile;
  const FillProfilePage({Key? key, required this.userProfile})
      : super(key: key);

  @override
  _FillProfilePageState createState() => _FillProfilePageState();
}

class _FillProfilePageState extends State<FillProfilePage> {
  @override
  Widget build(BuildContext context) {
    var profile = widget.userProfile;
    return SingleChildScrollView(
      padding: EdgeInsets.all(10),
      child: Column(
        children: [
          PublicUserProfileInfos(profileUID: profile.uid),
          Separateur(),
          PublicUserNeeds(profileUID: profile.uid),
          Separateur(),
          PublicUserSkills(
            profileUID: profile.uid,
          ),
          Separateur(),
          PublicUserOffers(profileUID: profile.uid),
        ],
      ),
    );
  }

  Widget Separateur() {
    return SizedBox(
      height: 20,
    );
  }
}

class PublicUserProfileInfos extends StatefulWidget {
  String profileUID;
  PublicUserProfileInfos({Key? key, required this.profileUID})
      : super(key: key);

  @override
  State<PublicUserProfileInfos> createState() => _PublicUserProfileInfosState();
}

class _PublicUserProfileInfosState extends State<PublicUserProfileInfos> {
  var _user = FirebaseAuth.instance.currentUser;
  var _ref = FirebaseFirestore.instance.collection('UserProfiles');
  @override
  Widget build(BuildContext context) {
    var profiledoc = _ref.doc(widget.profileUID).get();
    return Card(
      clipBehavior: Clip.antiAlias,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Container(
              width: double.infinity,
              color: darkGreenCol,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Profil',
                    style: bold_white_title,
                  ),
                  // Row(
                  //   mainAxisSize: MainAxisSize.min,
                  //   mainAxisAlignment: MainAxisAlignment.end,
                  //   children: [
                  //     IconButton(onPressed: () {}, icon: Icon(Icons.edit)),
                  //     IconButton(
                  //         onPressed: () {},
                  //         icon: Icon(
                  //           Icons.settings,
                  //           color: Colors.blueGrey,
                  //         )),
                  //   ],
                  // )
                ],
              )),
          FutureBuilder(
              future: _ref.doc(widget.profileUID).get(),
              builder: (context,
                  AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>>
                      snapshot) {
                if (snapshot.connectionState == ConnectionState.done &&
                    snapshot.hasData) {
                  Map<String, dynamic> data =
                      snapshot.data!.data() as Map<String, dynamic>;
                  return Column(
                    children: [
                      CircleAvatar(
                          backgroundColor:
                              PseudoAvatarColor(data['displayname']),
                          radius: 50,
                          child: Text(
                            data['displayname'][0] + data['displayname'][1],
                            style: TextStyle(color: Colors.white, fontSize: 50),
                          )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          // TODO : Name and first name
                          // Text(data['firstname'] as String),
                          // SizedBox(width: 5),
                          // Text(data['name'] as String),
                          Text(data['displayname'] as String),
                        ],
                      ),
                      Text(data['location'] as String),
                      // Row(
                      //   mainAxisAlignment: MainAxisAlignment.center,
                      //   children: [
                      //     Text('Expérimentateur' as String),
                      //     SizedBox(width: 10),
                      //     // TODO implémenter les points
                      //     Text('0 points'),
                      //   ],
                      // )
                    ],
                  );
                }

                return Text("Chargement");
              })
        ],
      ),
    );
  }
}
