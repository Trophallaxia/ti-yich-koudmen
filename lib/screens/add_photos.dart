import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

// firebase_storage.FirebaseStorage storage =
//     firebase_storage.FirebaseStorage.instance;

// firebase_storage.Reference ref =
//     firebase_storage.FirebaseStorage.instance.ref();

class AddPhotos extends StatefulWidget {
  const AddPhotos({Key? key}) : super(key: key);

  @override
  _AddPhotosState createState() => _AddPhotosState();
}

class _AddPhotosState extends State<AddPhotos> {
  String _imageUrl = '';

  void initState() {
    super.initState();
    K();
  }

  void K() async {
    var ref = firebase_storage.FirebaseStorage.instance.ref('Image1.png');
    await ref.getDownloadURL().then((loc) => setState(() => _imageUrl = loc));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
            child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Container(
            height: 150,
            width: 150,
            child: _imageUrl == ''
                ? Image.network(
                    'https://st.depositphotos.com/1052233/2885/v/950/depositphotos_28850541-stock-illustration-male-default-profile-picture.jpg)')
                : Image.network(
                    _imageUrl,
                    fit: BoxFit.cover,
                  ) //Image.network(downloadURL),
            ),
        ElevatedButton(
            onPressed: () {
              setState(() {});
            },
            child: Text('PRESS ME'))
      ],
    )));
  }
}
