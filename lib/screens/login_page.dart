import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

//TODO : Clean those import shit

import 'package:ti_yich_test/const.dart';
import 'package:ti_yich_test/home.dart';
import 'package:ti_yich_test/screens/dispatcher_page.dart';
import 'package:ti_yich_test/screens/onboarding_page.dart';
import 'package:ti_yich_test/screens/register_page.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  var _formkey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return InteractiveViewer(
      maxScale: 4,
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topCenter,
                    end: Alignment.bottomRight,
                    colors: [Color(0xFF74E2DB), Color(0xFFE6D8FC)])),
            child: SafeArea(
              child: Stack(children: [
                //Nuages en fond
                // whiteClouds(),
                Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        // Nom de l'application
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Text(
                              'Karisko',
                              style: TextStyle(fontSize: 64, color: purpleCol),
                            ),
                            Text(
                              'Ti Yich Koudmen',
                              style: TextStyle(
                                  fontSize: 32,
                                  color: purpleCol,
                                  fontWeight: FontWeight.w300),
                              textAlign: TextAlign.end,
                            ),
                          ],
                        ),
                        // Formulaires d'identification
                        Padding(
                          padding: const EdgeInsets.all(32.0),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Form(
                                key: _formkey,
                                child: Column(
                                  children: [
                                    TextFormField(
                                      controller: _emailController,
                                      keyboardType: TextInputType.emailAddress,
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Ce champ ne peut être vide';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        labelText: 'Identifiant',
                                        prefixIcon: Icon(Icons.person),
                                        fillColor: darkGreenCol,
                                        filled: true,
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    ),
                                    TextFormField(
                                      controller: _passwordController,
                                      obscureText: true,
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Ce champ ne peut être vide';
                                        } else if (value.length <= 5) {
                                          return 'Le mot de passe doit faire plus de 6 caractères';
                                        }
                                        return null;
                                      },
                                      decoration: InputDecoration(
                                        labelText: 'Mot de passe',
                                        prefixIcon: Icon(Icons.lock),
                                        fillColor: darkGreenCol,
                                        filled: true,
                                        border: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(10)),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              SizedBox(
                                height: 0,
                              ),
                              // Lien de mot de passe oublié
                              Align(
                                alignment: Alignment.centerRight,
                                child: TextButton(
                                    onPressed: () {
                                      print('Forgot Password pressed');
                                      showDialog(
                                        context: context,
                                        builder: (BuildContext context) =>
                                            _buildPopupDialog(context),
                                      );

                                      // ScaffoldMessenger.of(context).showSnackBar(
                                      //     SnackBar(content: Text('Error')));
                                    },
                                    child: Text(
                                      'Mot de passe oublié',
                                      style: TextStyle(
                                        color: softGreencol,
                                      ),
                                    )),
                              ),
                              ElevatedButtonTheme(
                                data: ElevatedButtonThemeData(
                                    style: ButtonStyle(
                                        backgroundColor:
                                            MaterialStateProperty.all(
                                                purpleCol))),
                                child: Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    ElevatedButton(
                                        onPressed: () async {
                                          print('Login pressed');
                                          if (_formkey.currentState!
                                              .validate()) {
                                            try {
                                              var signInWithEmailAndPassword =
                                                  await FirebaseAuth.instance
                                                      .signInWithEmailAndPassword(
                                                          email:
                                                              _emailController
                                                                  .value.text,
                                                          password:
                                                              _passwordController
                                                                  .value.text)
                                                      .then(
                                                        (value) => Navigator.push(
                                                            context,
                                                            MaterialPageRoute(
                                                                builder:
                                                                    (context) =>
                                                                        DispatcherPage())),
                                                      );
                                            } on FirebaseAuthException catch (e) {
                                              ScaffoldMessenger.of(context)
                                                  .showSnackBar(SnackBar(
                                                      content: Text(e.message
                                                          .toString())));
                                            }
                                            // .then(
                                            //   (value) => Navigator.push(
                                            //       context,
                                            //       MaterialPageRoute(
                                            //           builder: (context) =>
                                            //               DispatcherPage())),
                                            // )
                                            //   .catchError((e) {
                                            // ScaffoldMessenger.of(context)
                                            //     .showSnackBar(SnackBar(
                                            //         content: Text('Error')));
                                            // return null;
                                            // });

                                            // setState(() {});
                                          }
                                        },
                                        child: Text('Connexion')),
                                    // ElevatedButton(
                                    //     onPressed: () {
                                    //       print('Anonymous pressed');
                                    //       FirebaseAuth.instance
                                    //           .signInAnonymously()
                                    //           .then((value) => Navigator.push(
                                    //               context,
                                    //               MaterialPageRoute(
                                    //                   builder: (context) =>
                                    //                       DispatcherPage())));
                                    //     },
                                    //     child: Text('Connexion Anonyme')),
                                  ],
                                ),
                              )
                            ],
                          ),
                        ),
                        // Lien d'inscription
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Text('Pas encore membre ?',
                                  style: TextStyle(color: softGreencol)),
                              TextButton(
                                  onPressed: () {
                                    Navigator.of(context).push(
                                        MaterialPageRoute(
                                            builder: (BuildContext context) =>
                                                RegisterPage()));
                                    print('Register pressed');
                                  },
                                  child: Text(
                                    'Inscrivez vous maintenant !',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: purpleCol),
                                  ))
                            ]),
                      ],
                    ),
                  ),
                ),
              ]),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildPopupDialog(BuildContext context) {
    return new AlertDialog(
      title: const Text('Mot de passe oublié ?'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
              "Merci de contacter l'administrateur de l'application via le groupe whatsapp des testeurs"),
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text('Close'),
        ),
      ],
    );
  }
}
