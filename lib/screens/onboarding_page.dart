import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'package:smooth_page_indicator/smooth_page_indicator.dart';
import 'package:ti_yich_test/home.dart';
import 'package:ti_yich_test/const.dart';

class OnboardingScreen extends StatefulWidget {
  // static MaterialPage page() {
  //   return MaterialPage(
  //     name: FooderlichPages.onboardingPath,
  //     key: ValueKey(FooderlichPages.onboardingPath),
  //     child: const OnboardingScreen(),
  //   );
  // }

  const OnboardingScreen({Key? key}) : super(key: key);

  @override
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen> {
  final controller = PageController();
  PageController pageController = PageController();
  final Color rwColor = const Color.fromRGBO(64, 143, 77, 1);
  User? user = FirebaseAuth.instance.currentUser;
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) => PopUp());
    // pageController = PageController();
  }

  @override
  Widget build(BuildContext context) {
    return InteractiveViewer(
      maxScale: 4,
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: darkGreenCol,
          title: Center(
              child: Column(
            children: [
              Text("À quoi sert l'application"),
              Text(
                "Koudmen pour toi",
                style: bold_creole_title,
              ),
            ],
          )),
        ),
        body: SafeArea(
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Bienvenue ${user?.displayName ?? 'Anonymous'}',
                  style: bold_purple_title,
                ),
              ),
              Flexible(child: buildPages()),
              buildIndicator(),
              buildActionButtons(),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildActionButtons() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: [
        OutlinedButton(
          style: OutlinedButton.styleFrom(shape: StadiumBorder()),
          child: const Text(
            'Suivant',
            style: TextStyle(fontSize: 18, color: Colors.black),
          ),
          onPressed: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) => Home(
                          index: 0,
                        )));
          },
        ),
      ],
    );
  }

  Widget buildPages() {
    return PageView(
      controller: controller,
      children: [
        onboardPageView(
          const AssetImage('assets/images/voiture.jpg'),
          ''' De quoi as-tu besoin immédiatement?\n''',
        ),
        onboardPageView(const AssetImage('assets/images/thumbsup.jpg'),
            'Sois informé des koudmen autour de toi'),
        onboardPageView(const AssetImage('assets/images/discussiontel.jpg'),
            'Discute avec les autres utilisateurs pour organiser ton koudmen ou donner un ti koudmen'),
      ],
    );
  }

  Widget onboardPageView(ImageProvider imageProvider, String text) {
    return Padding(
      padding: const EdgeInsets.all(40),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Flexible(
            child: AspectRatio(
              aspectRatio: 1,
              child: Container(
                clipBehavior: Clip.antiAlias,
                decoration: roundCorners(Colors.white, 40),
                child: Image(
                  fit: BoxFit.fill,
                  image: imageProvider,
                ),
              ),
            ),
          ),
          const SizedBox(height: 16),
          Text(
            text,
            style: const TextStyle(fontSize: 20),
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }

  Widget buildIndicator() {
    return SmoothPageIndicator(
      controller: controller,
      count: 3,
      effect: WormEffect(activeDotColor: rwColor),
    );
  }

  int PopUp() {
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => new AlertDialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
        title: Center(
            child: Image(
          image: AssetImage('assets/images/undraw_Notify_re_65on.png'),
        )),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            new Text(
              "Bienvenue dans le projet Ti Yich Koudmen de KARISKO et la ville de Fort-de-France. \n \n Tu es dans une première version de l'application.",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: darkGreyCol,
              ),
            ),
            Text(
              " \n Sé yonn a-lot !\n",
              textAlign: TextAlign.center,
              style: bold_creole_title,
            ),
            Text(
              'Pour tout problème ou suggestion merci de nous contacter à : example@test.com',
              textAlign: TextAlign.center,
              style: TextStyle(
                color: darkGreyCol,
              ),
            )
          ],
        ),
      ),
    );
    return (0);
  }
}
