import 'package:flutter/material.dart';

// Size

// Colors
const blueCol = Color(0xFF74E2DB);
const whiteCol = Color(0xFFFFFFFF);
const purpleCol = Color(0xFF4b316e);
const darkGreyCol = Color(0XFF505050);
const lightGreyCol = Color(0XFF787878);
const formIconCol = Color(0xFFA0A0A0);
const lightPurpleCol = Color(0XFF5B4080);
const darkGreenCol = Color(0XFF2B7B75);
const softGreencol = Color(0XFF80CBC4);
const ligthGreenCol = Color(0XFFC8E9E7);

// Decoration

BoxDecoration backgroundGradient = BoxDecoration(
    gradient: LinearGradient(
        begin: Alignment.topCenter,
        end: Alignment.bottomRight,
        colors: [Color(0xFF74E2DB), Color(0xFFE6D8FC)]));

//Images

Widget whiteCloudImage = Image.asset("assets/images/whitecloud_1280x1024.png");
var logoKarisko = Image.asset("logo-karisko-moyen-300x236.png");

//Textstyles

TextStyle bold_purple_title =
    TextStyle(color: purpleCol, fontWeight: FontWeight.bold, fontSize: 24);

TextStyle bold_white_title =
    TextStyle(color: whiteCol, fontWeight: FontWeight.bold, fontSize: 24);

TextStyle bold_creole_title = TextStyle(
    color: Colors.orange.shade300, fontWeight: FontWeight.bold, fontSize: 24);

TextStyle grey_text = TextStyle(color: lightGreyCol, fontSize: 18);

TextStyle grey_text_smol_bold =
    TextStyle(color: lightGreyCol, fontSize: 14, fontWeight: FontWeight.bold);

//Widgets

class whiteClouds extends StatelessWidget {
  const whiteClouds({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Positioned(
          top: 50,
          left: -100,
          child: SizedBox(
            height: 200,
            width: 200,
            child: Image.asset("assets/images/whitecloud_1280x1024.png"),
          )),
      Positioned(
          bottom: 50,
          right: -100,
          child: SizedBox(
            height: 200,
            width: 200,
            child: Image.asset("assets/images/whitecloud_1280x1024.png"),
          )),
    ]);
  }
}

class PaidStatus extends StatefulWidget {
  bool status;
  PaidStatus(this.status, {Key? key}) : super(key: key);

  @override
  _PaidStatusState createState() => _PaidStatusState();
}

class _PaidStatusState extends State<PaidStatus> {
  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
        style: OutlinedButton.styleFrom(
          side: BorderSide(color: lightGreyCol, width: 1),
          fixedSize: Size(100, 30),
        ),
        onPressed: () {
          setState(() {
            widget.status = !widget.status;
          });
        },
        child: widget.status
            ? Text(
                "PAID",
                style: TextStyle(color: Colors.amber),
              )
            : Text('UNPAID'));
  }
}

BoxDecoration whiteColRoundCorners_4 = BoxDecoration(
  color: whiteCol,
  borderRadius: BorderRadius.circular(4),
  boxShadow: [
    BoxShadow(
      color: lightGreyCol,
      blurRadius: 2.0,
      spreadRadius: 0.0,
      offset: Offset(2.0, 2.0), // shadow direction: bottom right
    )
  ],
);

BoxDecoration whiteColRoundCorners_8 = BoxDecoration(
  color: whiteCol,
  borderRadius: BorderRadius.circular(8),
  boxShadow: [
    BoxShadow(
      color: lightGreyCol,
      blurRadius: 2.0,
      spreadRadius: 0.0,
      offset: Offset(2.0, 2.0), // shadow direction: bottom right
    )
  ],
);

BoxDecoration whiteColRoundCorners_20 = BoxDecoration(
  color: Colors.transparent,
  borderRadius: BorderRadius.circular(30),
  boxShadow: [
    BoxShadow(
      color: lightGreyCol,
      blurRadius: 2.0,
      spreadRadius: 0.0,
      offset: Offset(2.0, 2.0), // shadow direction: bottom right
    )
  ],
);

BoxDecoration roundCorners(Color color, double radius) {
  return BoxDecoration(
    color: color,
    borderRadius: BorderRadius.circular(radius),
    boxShadow: [
      BoxShadow(
        color: lightGreyCol,
        blurRadius: 2.0,
        spreadRadius: 0.0,
        offset: Offset(2.0, 2.0), // shadow direction: bottom right
      )
    ],
  );
}
