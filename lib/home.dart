import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:ti_yich_test/const.dart';
import 'package:ti_yich_test/data/user_dao.dart';
import 'package:ti_yich_test/models/user_profile_data.dart';
import 'package:ti_yich_test/models/users_needs.dart';
import 'package:ti_yich_test/models/users_offers.dart';
import 'package:ti_yich_test/models/users_skills.dart';
import 'package:ti_yich_test/screens/add_photos.dart';
import 'package:ti_yich_test/screens/db_test.dart';
import 'package:ti_yich_test/screens/dispatcher_page.dart';
import 'package:ti_yich_test/screens/public_profile_page.dart';
import 'screens/chat_page.dart';
import 'screens/profile_page.dart';
import 'screens/newsfeed_page.dart';
import 'package:provider/provider.dart';

var _auth = FirebaseAuth.instance;

FirebaseStorage storage = FirebaseStorage.instance;
var imageURL = storage.ref('Image1.png').getDownloadURL();

class Home extends StatefulWidget {
  int index;
  Home({Key? key, required this.index}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  GlobalKey<_HomeState> homekey = GlobalKey();

  PageController pageController = PageController();
  List<Widget> _screensList = [NewsFeedPage(), ProfilePage(), ChatPage()];

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance!
        .addPostFrameCallback((_) => setindex(widget.index));
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    int _selectedindex = widget.index;
    return InteractiveViewer(
      maxScale: 4,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: darkGreenCol,
          leading: Padding(
            padding: const EdgeInsets.all(5.0),
            child: GestureDetector(
              onTap: () {
                // Navigator.of(context)
                //     .push(MaterialPageRoute(builder: (_) => AddPhotos()));
              },
              child: CircleAvatar(
                backgroundImage:
                    AssetImage('assets/images/logo-karisko-moyen-300x236.png'),
              ),
            ),
          ),
          actions: [
            Column(
              children: [
                Flexible(
                  child: IconButton(
                      tooltip: 'Déconnexion',
                      onPressed: () {
                        var userDao = context.read<UserDao>();
                        userDao.logout();
                        Navigator.of(context).pushAndRemoveUntil(
                            MaterialPageRoute(
                                builder: (context) => DispatcherPage()),
                            (Route<dynamic> route) => false);
                      },
                      icon: Icon(Icons.logout)),
                ),
                Text('Déconnexion  '),
              ],
            )
          ],
          title: Text(_auth.currentUser?.displayName ?? 'Anonymous'),
        ),
        body: SafeArea(
          child: Container(
              // Décoration de la page (dégradé)
              decoration: backgroundGradient,

              // Widgets du corps de la page
              child: PageView(
                controller: pageController,
                onPageChanged: (index) {},
                children: _screensList,
              )),
        ),
        bottomNavigationBar: BottomNavigationBar(
          backgroundColor: darkGreenCol,
          selectedItemColor: ligthGreenCol,
          unselectedItemColor: softGreencol,
          currentIndex: _selectedindex,
          onTap: setindex,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.near_me),
              label: "Fil d'actualité",
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              label: 'Profil',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.chat),
              label: 'Messagerie',
            ),
          ],
        ),
      ),
    );
  }

  void setindex(int index) {
    setState(() {
      widget.index = index;

      pageController.animateToPage(index,
          duration: Duration(milliseconds: 500), curve: Curves.easeOut);
    });
  }
}
