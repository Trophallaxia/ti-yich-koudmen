import 'package:cloud_firestore/cloud_firestore.dart';
import 'feed.dart';

class FeedDao {
  // 1
  final CollectionReference collection =
      FirebaseFirestore.instance.collection('Feed');

  // TODO: Add saveMessage
  void saveFeedItem(FeedItem feedItem) {
    collection.add(feedItem.toJson());
  }

// TODO: Add getMessageStream
  Stream<QuerySnapshot> getFeedStream() {
    return collection.snapshots();
  }
}
