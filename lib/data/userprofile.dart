import 'package:cloud_firestore/cloud_firestore.dart';

class UserProfile {
  String uid;
  String nom;
  String prenom;
  String location;
  List<dynamic>? skills;
  // List<Map<String, String>> offers = [];
  List<dynamic>? needs;

  DocumentReference? reference;

  UserProfile({
    required this.uid,
    required this.nom,
    required this.prenom,
    required this.location,
    this.reference,
    this.skills,
    this.needs,
  });

  factory UserProfile.fromJson(Map<dynamic, dynamic> json) => UserProfile(
        uid: json['uid'] as String,
        nom: json['name'] as String,
        prenom: json['firstname'] as String,
        location: json['location'] as String,
        skills: json['skills'] != null ? json['skills'] as List<dynamic> : [],
        needs: json['needs'] != null ? json['needs'] as List<dynamic> : [],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'uid': uid,
        'nom': nom,
        'prenom': prenom,
        'location': location,
        'skills': skills ?? [],
        'needs': needs ?? [],
      };

  factory UserProfile.fromSnapshot(DocumentSnapshot snapshot) {
    final userProfile =
        UserProfile.fromJson(snapshot.data() as Map<String, dynamic>);
    userProfile.reference = snapshot.reference;
    return userProfile;
  }
}
