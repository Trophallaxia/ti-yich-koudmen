import 'package:cloud_firestore/cloud_firestore.dart';

class FeedItem {
  final Map<String, dynamic> content;
  final DateTime date;
  final String type;
  final String uid;

  DocumentReference? reference;
  List<dynamic> participants;

  FeedItem(
      {required this.content,
      required this.date,
      required this.type,
      required this.uid,
      this.reference,
      this.participants = const []});

  // TODO: Add JSON converters
  factory FeedItem.fromJson(Map<dynamic, dynamic> json) => FeedItem(
      content: json['content'] as Map<String, dynamic>,
      date: DateTime.parse(json['date'] as String),
      type: json['type'] as String,
      uid: json['uid'] as String,
      participants: json['participants'] == null
          ? []
          : json['participants'] as List<dynamic>);

  Map<String, dynamic> toJson() => <String, dynamic>{
        'date': date.toString(),
        'content': content /*toString ?*/,
        'type': type,
        'uid': uid,
      };

// TODO: Add fromSnapshot
  factory FeedItem.fromSnapshot(DocumentSnapshot snapshot) {
    final feedItem = FeedItem.fromJson(snapshot.data() as Map<String, dynamic>);
    feedItem.reference = snapshot.reference;
    return feedItem;
  }
}
