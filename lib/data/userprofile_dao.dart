import 'package:cloud_firestore/cloud_firestore.dart';

class UserProfileDao {
  final CollectionReference collection =
      FirebaseFirestore.instance.collection('UserProfiles');

  UserProfileDao();

  // void saveProfile(UserProfile profile) {
  //   collection.doc(doc_id).set(profile.toJson());
  // }

  // void updateUserProfile(UserProfile profile) {
  //   collection.doc(doc_id).update(profile.toJson());
  // }
  Future<DocumentSnapshot<Object?>> getUserProfile(String uid) {
    return collection.doc(uid).get();
  }
}
