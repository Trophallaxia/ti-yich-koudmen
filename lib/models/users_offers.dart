import 'package:flutter/material.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'user_profile_data.dart';

//TODO: Fix that import const shit
import 'package:ti_yich_test/const.dart';
import 'misc_functions.dart';

// var user_needs = [
//   {
//     'title': "Faire un jardin créole",
//     'slot': "Lun-Ven",
//     'details': "Je n'ai pas la main verte :("
//   },
//   {
//     'title': "Remplir ma fiche d'impôts",
//     'slot': "Sam-Dim",
//     'details': "Je n'y comprends rien"
//   }
// ];

class UserOffers extends StatefulWidget {
  String profileUID;
  UserOffers({Key? key, required this.profileUID}) : super(key: key);

  final needs_textcontroller = TextEditingController();

  @override
  State<UserOffers> createState() => _UserOffersState();
}

class _UserOffersState extends State<UserOffers> {
  var _auth = FirebaseAuth.instance;

  var _ref = FirebaseFirestore.instance.collection('UserProfiles');
  var need_to_add = '';
  var _stackindex = 0;
  @override
  Widget build(BuildContext context) {
    // var _user = widget.profileUID;
    var temp = 0;
    return Card(
      clipBehavior: Clip.antiAlias,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
                width: double.infinity,
                color: darkGreenCol,
                child: Center(
                    child: Column(
                  children: [
                    Text(
                      'Mes offres koudmen',
                      style: bold_white_title,
                    ),
                    Text(
                      'Sa mwen pé poté baw ',
                      style: bold_creole_title,
                    )
                  ],
                ))),
            TabChoice(),
            BuildOffersList(_stackindex),
            AddOffer(_stackindex),
          ],
        ),
      ),
    );
  }

  bool _is_valid_entry(List<Map> data, String entry) {
    if (entry == '' ||
        data.any((element) {
          return element.containsValue(entry);
        })) return false;

    return true;
  }

  Widget AddOffer(int index) {
    var title_textcontroller = TextEditingController();
    var slot_textcontroller = TextEditingController();
    var details_textcontroller = TextEditingController();
    var data = index == 0 ? user_service_offers : user_gears_offers;
    return ExpansionTile(
      controlAffinity: ListTileControlAffinity.leading,
      trailing: IconButton(
          icon: Icon(Icons.add_circle),
          onPressed: () async {
            if (_is_valid_entry(data, title_textcontroller.text)) {
              data.insert(0, {
                'title': title_textcontroller.text,
                'slot': slot_textcontroller.text,
                'details': details_textcontroller.text,
              });
              print('not added to feed');
              String docID = await AddToFeed({
                'title': title_textcontroller.text,
                'slot': slot_textcontroller.text,
                'details': details_textcontroller.text,
              }, index == 0 ? 'service_offer' : 'gears_offer',
                  widget.profileUID);
              print('added to feed');

              _ref.doc(widget.profileUID).set({
                index == 0 ? 'service_offers' : 'tools_offers':
                    FieldValue.arrayUnion([
                  {
                    'title': title_textcontroller.text,
                    'slot': slot_textcontroller.text,
                    'details': details_textcontroller.text,
                    'feedID': docID,
                  }
                ])
              }, SetOptions(merge: true)).then(
                  (value) => print('added to profile'));
            }

            FocusScope.of(context).unfocus();
          }),
      title: Text('Ajouter une offre'),
      children: [
        ListTile(
          leading: Text('Offre :'),
          title: TextField(
            maxLines: 2,
            controller: title_textcontroller,
            decoration: InputDecoration(
                hintText: 'Ex: Cours de dessin, cours de guitare'),
          ),
        ),
        ListTile(
          leading: Text('Créneau :'),
          title: TextField(
            maxLines: 2,
            controller: slot_textcontroller,
            decoration: InputDecoration(
                hintText: 'Vos disponibilté, ex: Lun-Ven, Mardi + Jeudi'),
          ),
        ),
        ListTile(
          leading: Text('Détails :'),
          title: TextField(
            controller: details_textcontroller,
            maxLines: 5,
            maxLength: 300,
            decoration: InputDecoration(hintText: 'Précisez votre offre ici'),
          ),
        )
      ],
    );
  }

  Widget BuildOffersList(int index) {
    var data = index == 0 ? user_service_offers : user_gears_offers;
    var _offerType = index == 0 ? 'service_offers' : 'tools_offers';

    return StreamBuilder(
        stream: _ref.doc(widget.profileUID).snapshots(),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting ||
              !snapshot.hasData) {
            return Text("Loading");
          }

          if (snapshot.data == null) {
            return Text("No Data yet");
          }

          // Map<String, dynamic> data =
          //     snapshot.data?.data() as Map<String, dynamic> ?? {};

          Map<String, dynamic> data = snapshot.data?.data() != null
              ? snapshot.data?.data() as Map<String, dynamic>
              : {};
          List offersList = data[_offerType] ?? [];

          return ListView.builder(
              padding: EdgeInsets.all(0),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: offersList.length,
              itemBuilder: (context, index) {
                return ExpansionTile(
                  childrenPadding: EdgeInsets.all(10),
                  controlAffinity: ListTileControlAffinity.leading,
                  title: Text(offersList[index]['title']!),
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Créneau: '),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(offersList[index]['slot']!),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Détails: '),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(offersList[index]['details']!),
                        )
                      ],
                    )
                  ],
                  trailing: IconButton(
                      onPressed: () {
                        setState(() {
                          RemoveFromFeed(offersList[index]['feedID']);
                          _ref.doc(widget.profileUID).set({
                            _offerType:
                                FieldValue.arrayRemove([offersList[index]])
                          }, SetOptions(merge: true));
                        });
                      },
                      icon: Icon(Icons.remove_circle)),
                );
              });
        });

    // return ListView.builder(
    //     padding: EdgeInsets.all(0),
    //     shrinkWrap: true,
    //     physics: NeverScrollableScrollPhysics(),
    //     itemCount: data.length,
    //     itemBuilder: (BuildContext context, int index) {
    //       return ExpansionTile(
    //         childrenPadding: EdgeInsets.all(10),
    //         controlAffinity: ListTileControlAffinity.leading,
    //         title: Text(data[index]['title'] ?? ''),
    //         children: [
    //           Row(
    //             crossAxisAlignment: CrossAxisAlignment.start,
    //             children: [
    //               Text('Créneau: '),
    //               SizedBox(
    //                 width: 10,
    //               ),
    //               Expanded(
    //                 child: Text(data[index]['slot'] ?? ''),
    //               )
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             crossAxisAlignment: CrossAxisAlignment.start,
    //             children: [
    //               Text('Détails: '),
    //               SizedBox(
    //                 width: 10,
    //               ),
    //               Expanded(
    //                 child: Text(data[index]['details'] ?? ''),
    //               )
    //             ],
    //           )
    //         ],
    //         trailing: IconButton(
    //             onPressed: () {
    //               setState(() {
    //                 data.remove(data[index]);
    //               });
    //             },
    //             icon: Icon(Icons.remove_circle)),
    //       );
    //     });
  }

  Widget TabChoice() {
    return Container(
        // decoration: whiteColRoundCorners_20,
        // clipBehavior: Clip.antiAlias,
        width: double.infinity,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            // Flexible(
            //   child: GestureDetector(
            //     onTap: () => setState(() {
            //       _stackindex = 0;
            //     }),
            //     child: Container(
            //       height: 75,
            //       width: double.infinity,
            //       color: FocusedContainterColor(_stackindex),
            //       child: Center(
            //           child: Text(
            //         'Services 🤝',
            //         style: FocusedTextStyle(_stackindex),
            //       )),
            //     ),
            //   ),
            // ),
            // Flexible(
            //   child: GestureDetector(
            //     onTap: () => setState(() {
            //       _stackindex = 1;
            //     }),
            //     child: Container(
            //       height: 75,
            //       width: double.infinity,
            //       color: FocusedContainterColor((_stackindex + 1) % 2),
            //       child: Center(
            //           child: Text('Matériel 🛠',
            //               style: FocusedTextStyle((_stackindex + 1) % 2))),
            //     ),
            //   ),
            // )
          ],
        ));
  }

  Color FocusedContainterColor(var index) {
    return index == 0 ? whiteCol : darkGreenCol;
  }

  TextStyle FocusedTextStyle(var index) {
    return index == 0
        ? TextStyle(
            color: lightGreyCol, fontWeight: FontWeight.normal, fontSize: 24)
        : TextStyle(color: whiteCol, fontWeight: FontWeight.bold, fontSize: 24);
  }
}

// PUBLIC SCREEN

class PublicUserOffers extends StatefulWidget {
  String profileUID;
  PublicUserOffers({Key? key, required this.profileUID}) : super(key: key);

  final needs_textcontroller = TextEditingController();

  @override
  State<PublicUserOffers> createState() => _PublicUserOffersState();
}

class _PublicUserOffersState extends State<PublicUserOffers> {
  var _auth = FirebaseAuth.instance;

  var _ref = FirebaseFirestore.instance.collection('UserProfiles');

  var _stackindex = 0;
  @override
  Widget build(BuildContext context) {
    // var _user = widget.profileUID;
    var temp = 0;
    return Card(
      clipBehavior: Clip.antiAlias,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
                width: double.infinity,
                color: darkGreenCol,
                child: Center(
                    child: Column(
                  children: [
                    Text(
                      'Mes offres koudmen',
                      style: bold_white_title,
                    ),
                    Text(
                      'Sa mwen pé poté baw ',
                      style: bold_creole_title,
                    )
                  ],
                ))),
            // TabChoice(),
            BuildOffersList(_stackindex),
            SizedBox(
              height: 10,
            )
          ],
        ),
      ),
    );
  }

  Widget BuildOffersList(int index) {
    var data = index == 0 ? user_service_offers : user_gears_offers;
    var _offerType = index == 0 ? 'service_offers' : 'tools_offers';

    return StreamBuilder(
        stream: _ref.doc(widget.profileUID).snapshots(),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>> snapshot) {
          if (snapshot.hasError) {
            return Text('Une erreur est survenue');
          }

          if (snapshot.connectionState == ConnectionState.waiting ||
              !snapshot.hasData) {
            return Text("Chargement");
          }

          if (snapshot.data == null) {
            return Text("Pas de données");
          }

          // Map<String, dynamic> data =
          //     snapshot.data?.data() as Map<String, dynamic> ?? {};

          Map<String, dynamic> data = snapshot.data?.data() != null
              ? snapshot.data?.data() as Map<String, dynamic>
              : {};
          List offersList = data[_offerType] ?? [];

          return ListView.builder(
              padding: EdgeInsets.all(0),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: offersList.length,
              itemBuilder: (context, index) {
                return ExpansionTile(
                  childrenPadding: EdgeInsets.all(10),
                  controlAffinity: ListTileControlAffinity.leading,
                  title: Text(offersList[index]['title']!),
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Créneau: '),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(offersList[index]['slot']!),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Détails: '),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(offersList[index]['details']!),
                        )
                      ],
                    )
                  ],
                  trailing: IconButton(
                      onPressed: () {
                        setState(() {
                          RemoveFromFeed(offersList[index]['feedID']);
                          _ref.doc(widget.profileUID).set({
                            _offerType:
                                FieldValue.arrayRemove([offersList[index]])
                          }, SetOptions(merge: true));
                        });
                      },
                      icon: Icon(Icons.remove_circle)),
                );
              });
        });

    // return ListView.builder(
    //     padding: EdgeInsets.all(0),
    //     shrinkWrap: true,
    //     physics: NeverScrollableScrollPhysics(),
    //     itemCount: data.length,
    //     itemBuilder: (BuildContext context, int index) {
    //       return ExpansionTile(
    //         childrenPadding: EdgeInsets.all(10),
    //         controlAffinity: ListTileControlAffinity.leading,
    //         title: Text(data[index]['title'] ?? ''),
    //         children: [
    //           Row(
    //             crossAxisAlignment: CrossAxisAlignment.start,
    //             children: [
    //               Text('Créneau: '),
    //               SizedBox(
    //                 width: 10,
    //               ),
    //               Expanded(
    //                 child: Text(data[index]['slot'] ?? ''),
    //               )
    //             ],
    //           ),
    //           SizedBox(
    //             height: 10,
    //           ),
    //           Row(
    //             crossAxisAlignment: CrossAxisAlignment.start,
    //             children: [
    //               Text('Détails: '),
    //               SizedBox(
    //                 width: 10,
    //               ),
    //               Expanded(
    //                 child: Text(data[index]['details'] ?? ''),
    //               )
    //             ],
    //           )
    //         ],
    //         trailing: IconButton(
    //             onPressed: () {
    //               setState(() {
    //                 data.remove(data[index]);
    //               });
    //             },
    //             icon: Icon(Icons.remove_circle)),
    //       );
    //     });
  }

  Widget TabChoice() {
    return Container(
        // decoration: whiteColRoundCorners_20,
        // clipBehavior: Clip.antiAlias,
        width: double.infinity,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: [
            Flexible(
              child: GestureDetector(
                onTap: () => setState(() {
                  _stackindex = 0;
                }),
                child: Container(
                  height: 75,
                  width: double.infinity,
                  color: FocusedContainterColor(_stackindex),
                  child: Center(
                      child: Text(
                    'Services 🤝',
                    style: FocusedTextStyle(_stackindex),
                  )),
                ),
              ),
            ),
            Flexible(
              child: GestureDetector(
                onTap: () => setState(() {
                  _stackindex = 1;
                }),
                child: Container(
                  height: 75,
                  width: double.infinity,
                  color: FocusedContainterColor((_stackindex + 1) % 2),
                  child: Center(
                      child: Text('Matériel 🛠',
                          style: FocusedTextStyle((_stackindex + 1) % 2))),
                ),
              ),
            )
          ],
        ));
  }

  Color FocusedContainterColor(var index) {
    return index == 0 ? whiteCol : darkGreenCol;
  }

  TextStyle FocusedTextStyle(var index) {
    return index == 0
        ? TextStyle(
            color: lightGreyCol, fontWeight: FontWeight.normal, fontSize: 24)
        : TextStyle(color: whiteCol, fontWeight: FontWeight.bold, fontSize: 24);
  }
}
