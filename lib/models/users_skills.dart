import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:ti_yich_test/const.dart';
import 'package:ti_yich_test/data/message.dart';
import 'package:ti_yich_test/data/message_dao.dart';
import 'package:ti_yich_test/data/userprofile.dart';
import 'user_profile_data.dart';

class UserSkills extends StatefulWidget {
  String profileUID;
  UserSkills({Key? key, required this.profileUID}) : super(key: key);

  @override
  State<UserSkills> createState() => _UserSkillsState();
}

class _UserSkillsState extends State<UserSkills> {
  var skill_to_add = '';
  var skill_textcontroller = TextEditingController();
  var _auth = FirebaseAuth.instance;

  var _ref = FirebaseFirestore.instance.collection('UserProfiles');

  @override
  Widget build(BuildContext context) {
    var _user = widget.profileUID;
    var temp = 0;
    return Card(
      clipBehavior: Clip.antiAlias,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
                width: double.infinity,
                color: darkGreenCol,
                child: Center(
                    child: Column(
                  children: [
                    Text(
                      'Mes compétences',
                      style: bold_white_title,
                    ),
                    Text(
                      'Sa mwen sa fè ',
                      style: bold_creole_title,
                    ),
                  ],
                ))),
            StreamBuilder(
              stream: _ref.doc(_user).snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>>
                      snapshot) {
                if (snapshot.hasError) {
                  return Text("Une erreur s'est produite");
                }

                if (snapshot.connectionState == ConnectionState.waiting ||
                    !snapshot.hasData) {
                  return Text("Chargement");
                }

                if (snapshot.data == null) {
                  return Text("Pas de données");
                }

                // Map<String, dynamic> data =
                //     snapshot.data?.data() as Map<String, dynamic> ?? {};

                Map<String, dynamic> data = snapshot.data?.data() != null
                    ? snapshot.data?.data() as Map<String, dynamic>
                    : {};
                List skillList = data['skills'] ?? [];

                return ListView.builder(
                    padding: EdgeInsets.all(0),
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: skillList.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(skillList[index]),
                        trailing: IconButton(
                            onPressed: () {
                              setState(() {
                                _ref.doc(_user).set({
                                  'skills': FieldValue.arrayRemove(
                                      [skillList[index].toString()])
                                }, SetOptions(merge: true));
                              });
                            },
                            icon: Icon(Icons.remove_circle)),
                      );
                    });
              },
            ),
            ListTile(
              trailing: IconButton(
                  icon: Icon(Icons.add_circle),
                  onPressed: () {
                    setState(() {
                      if (_is_valid_skill(skill_to_add)) {
                        // user_skills_list.insert(0, skill_to_add);

                        // userprofile.skills.add(skill_to_add);
                        // print(userprofile.skills);
                        // var userProfileDao = UserProfileDao(user!.uid);
                        // userProfileDao.saveProfile(userprofile);
                        // _ref
                        //     .update({'comp': skill_to_add})
                        //     .then((value) => print("c'est bon"))
                        //     .catchError((error) =>
                        //         print("Failed to add skill: $error"));
                        // setState(() {});

                        _ref.doc(_user).set({
                          'skills':
                              FieldValue.arrayUnion([skill_to_add.toString()])
                        }, SetOptions(merge: true));
                        user_skills_list.add(skill_to_add);
                      }
                    });
                    skill_textcontroller.clear();
                    FocusScope.of(context).unfocus();
                  }),
              title: TextField(
                controller: skill_textcontroller,
                decoration: InputDecoration(hintText: 'Ajouter une compétence'),
                onChanged: (value) => skill_to_add = value,
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool _is_valid_skill(String entry) {
    if (entry == '' || user_skills_list.contains(entry)) return false;

    return true;
  }
}

class PublicUserSkills extends StatefulWidget {
  String profileUID;
  PublicUserSkills({Key? key, required this.profileUID}) : super(key: key);

  @override
  State<PublicUserSkills> createState() => _PublicUserSkillsState();
}

class _PublicUserSkillsState extends State<PublicUserSkills> {
  var _ref = FirebaseFirestore.instance.collection('UserProfiles');

  @override
  Widget build(BuildContext context) {
    var _user = widget.profileUID;
    var temp = 0;
    return Card(
      clipBehavior: Clip.antiAlias,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
                width: double.infinity,
                color: darkGreenCol,
                child: Center(
                    child: Column(
                  children: [
                    Text(
                      'Mes compétences',
                      style: bold_white_title,
                    ),
                    Text(
                      'Sa mwen sa fè ',
                      style: bold_creole_title,
                    ),
                  ],
                ))),
            StreamBuilder(
              stream: _ref.doc(_user).snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>>
                      snapshot) {
                if (snapshot.hasError) {
                  return Text('Une erreur est survenue');
                }

                if (snapshot.connectionState == ConnectionState.waiting ||
                    !snapshot.hasData) {
                  return Text("Chargement");
                }

                if (snapshot.data == null) {
                  return Text("Pas de données");
                }

                // Map<String, dynamic> data =
                //     snapshot.data?.data() as Map<String, dynamic> ?? {};

                Map<String, dynamic> data = snapshot.data?.data() != null
                    ? snapshot.data?.data() as Map<String, dynamic>
                    : {};
                List skillList = data['skills'] ?? [];

                return ListView.builder(
                    padding: EdgeInsets.all(0),
                    shrinkWrap: true,
                    physics: NeverScrollableScrollPhysics(),
                    itemCount: skillList.length,
                    itemBuilder: (context, index) {
                      return ListTile(
                        title: Text(skillList[index]),
                      );
                    });
              },
            ),
            SizedBox(
              height: 10,
            )
          ],
        ),
      ),
    );
  }
}
