import '../screens/newsfeed_page.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

var _ref = FirebaseFirestore.instance.collection('Feed');

Future<String> AddToFeed(Map data, String type, String userId) async {
  var docRef = await _ref.add({
    'uid': userId,
    'type': type,
    'date': DateTime.now().toString(),
    'content': data,
  });
  return docRef.id;
}

void RemoveFromFeed(String id) {
  _ref.doc(id).delete();
}

Color AvatarColor(String firstname, String name) {
  var index = (firstname.codeUnitAt(0) + name.codeUnitAt(0)) % 18;
  return Colors.primaries[index];
}

Color PseudoAvatarColor(String pseudo) {
  var index = (pseudo.codeUnitAt(0) + pseudo.codeUnitAt(1)) % 18;
  return Colors.primaries[index];
}
