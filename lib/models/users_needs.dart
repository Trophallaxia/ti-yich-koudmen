import 'package:flutter/material.dart';
import 'package:ti_yich_test/models/misc_functions.dart';
import 'user_profile_data.dart';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'misc_functions.dart';
import 'package:ti_yich_test/const.dart';

class UserNeeds extends StatefulWidget {
  String profileUID;
  UserNeeds({Key? key, required this.profileUID}) : super(key: key);

  final needs_textcontroller = TextEditingController();

  @override
  State<UserNeeds> createState() => _UserNeedsState();
}

class _UserNeedsState extends State<UserNeeds> {
  var _auth = FirebaseAuth.instance;
  // var _user = FirebaseAuth.instance.currentUser;
  var _ref = FirebaseFirestore.instance.collection('UserProfiles');

  var need_to_add = '';
  @override
  Widget build(BuildContext context) {
    var temp = 0;
    return Card(
      clipBehavior: Clip.antiAlias,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              color: darkGreenCol,
              child: Center(
                child: Column(
                  children: [
                    Text(
                      'Mon besoin de koudmen',
                      style: bold_white_title,
                    ),
                    Text(
                      'Sa mwen lé',
                      style: bold_creole_title,
                    )
                  ],
                ),
              ),
            ),
            BuildNeedsList(),
            AddNeed(),
          ],
        ),
      ),
    );
  }

  bool _is_valid_entry(String entry) {
    if (entry == '' ||
        user_needs.any((element) {
          return element.containsValue(entry);
        })) return false;

    return true;
  }

  Widget AddNeed() {
    var title_textcontroller = TextEditingController();
    var slot_textcontroller = TextEditingController();
    var details_textcontroller = TextEditingController();
    return ExpansionTile(
      controlAffinity: ListTileControlAffinity.leading,
      trailing: IconButton(
          icon: Icon(Icons.add_circle),
          onPressed: () {
            setState(() {
              if (_is_valid_entry(title_textcontroller.text)) {
                user_needs.insert(0, {
                  'title': title_textcontroller.text,
                  'slot': slot_textcontroller.text,
                  'details': details_textcontroller.text,
                });

                _ref.doc(widget.profileUID).set({
                  'needs': FieldValue.arrayUnion([
                    {
                      'title': title_textcontroller.text,
                      'slot': slot_textcontroller.text,
                      'details': details_textcontroller.text,
                    }
                  ])
                }, SetOptions(merge: true)).then(
                    (value) => print('Button pressed'));

                AddToFeed({
                  'title': title_textcontroller.text,
                  'slot': slot_textcontroller.text,
                  'details': details_textcontroller.text,
                }, 'need', widget.profileUID as String);
              }

              widget.needs_textcontroller.clear();
            });
            print(user_needs);
            FocusScope.of(context).unfocus();
          }),
      title: Text('Ajouter un besoin koudmen'),
      children: [
        ListTile(
          leading: Text('Souhait :'),
          title: TextField(
            maxLines: 2,
            controller: title_textcontroller,
            decoration: InputDecoration(
                hintText:
                    'Ex: Créer un jardin potager, découvrir un instrument'),
          ),
        ),
        ListTile(
          leading: Text('Créneau :'),
          title: TextField(
            maxLines: 2,
            controller: slot_textcontroller,
            decoration: InputDecoration(
                hintText: 'Vos disponibilté, ex: Lun-Ven, Mardi + Jeudi'),
          ),
        ),
        ListTile(
          leading: Text('Détails :'),
          title: TextField(
            controller: details_textcontroller,
            maxLines: 5,
            maxLength: 300,
            decoration: InputDecoration(hintText: 'Précisez votre besoins ici'),
          ),
        )
      ],
    );
  }

  Widget BuildNeedsList() {
    return StreamBuilder(
        stream: _ref.doc(widget.profileUID).snapshots(),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting ||
              !snapshot.hasData) {
            return Text("Loading");
          }

          if (snapshot.data == null) {
            return Text("No Data yet");
          }

          // Map<String, dynamic> data =
          //     snapshot.data?.data() as Map<String, dynamic> ?? {};

          Map<String, dynamic> data = snapshot.data?.data() != null
              ? snapshot.data?.data() as Map<String, dynamic>
              : {};
          List needList = data['needs'] ?? [];

          return ListView.builder(
              padding: EdgeInsets.all(0),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: needList.length,
              itemBuilder: (context, index) {
                return ExpansionTile(
                  childrenPadding: EdgeInsets.all(10),
                  controlAffinity: ListTileControlAffinity.leading,
                  title: Text(needList[index]['title']!),
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Créneau: '),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(needList[index]['slot']!),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Détails: '),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(needList[index]['details']!),
                        )
                      ],
                    )
                  ],
                  trailing: IconButton(
                      onPressed: () {
                        setState(() {
                          _ref.doc(widget.profileUID).set({
                            'needs': FieldValue.arrayRemove([needList[index]])
                          }, SetOptions(merge: true));
                        });
                      },
                      icon: Icon(Icons.remove_circle)),
                );
              });
        });
  }
}

class PublicUserNeeds extends StatefulWidget {
  String profileUID;
  PublicUserNeeds({Key? key, required this.profileUID}) : super(key: key);

  @override
  State<PublicUserNeeds> createState() => _PublicUserNeedsState();
}

class _PublicUserNeedsState extends State<PublicUserNeeds> {
  var _ref = FirebaseFirestore.instance.collection('UserProfiles');

  @override
  Widget build(BuildContext context) {
    var temp = 0;
    return Card(
      clipBehavior: Clip.antiAlias,
      child: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              color: darkGreenCol,
              child: Center(
                child: Column(
                  children: [
                    Text(
                      'Mon besoin de koudmen',
                      style: bold_white_title,
                    ),
                    Text(
                      'Sa mwen lé',
                      style: bold_creole_title,
                    )
                  ],
                ),
              ),
            ),
            BuildNeedsList(),
            SizedBox(
              height: 10,
            )
          ],
        ),
      ),
    );
  }

  Widget BuildNeedsList() {
    return StreamBuilder(
        stream: _ref.doc(widget.profileUID).snapshots(),
        builder: (BuildContext context,
            AsyncSnapshot<DocumentSnapshot<Map<String, dynamic>>> snapshot) {
          if (snapshot.hasError) {
            return Text('Something went wrong');
          }

          if (snapshot.connectionState == ConnectionState.waiting ||
              !snapshot.hasData) {
            return Text("Loading");
          }

          if (snapshot.data == null) {
            return Text("No Data yet");
          }

          Map<String, dynamic> data = snapshot.data?.data() != null
              ? snapshot.data?.data() as Map<String, dynamic>
              : {};
          List needList = data['needs'] ?? [];

          return ListView.builder(
              padding: EdgeInsets.all(0),
              shrinkWrap: true,
              physics: NeverScrollableScrollPhysics(),
              itemCount: needList.length,
              itemBuilder: (context, index) {
                return ExpansionTile(
                  childrenPadding: EdgeInsets.all(10),
                  controlAffinity: ListTileControlAffinity.leading,
                  title: Text(needList[index]['title']!),
                  children: [
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Créneau: '),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(needList[index]['slot']!),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Détails: '),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Text(needList[index]['details']!),
                        )
                      ],
                    )
                  ],
                  trailing: IconButton(
                      onPressed: () {
                        setState(() {
                          _ref.doc(widget.profileUID).set({
                            'needs': FieldValue.arrayRemove([needList[index]])
                          }, SetOptions(merge: true));
                        });
                      },
                      icon: Icon(Icons.remove_circle)),
                );
              });
        });
  }
}
