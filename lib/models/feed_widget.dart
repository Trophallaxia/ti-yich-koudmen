import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_signin_button/button_list.dart';
import 'package:intl/intl.dart';
import 'package:ti_yich_test/data/message.dart';
import 'package:ti_yich_test/home.dart';
import 'package:ti_yich_test/models/misc_functions.dart';
import 'package:ti_yich_test/screens/participants_page.dart';
import 'package:ti_yich_test/screens/public_profile_page.dart';
import 'package:ti_yich_test/screens/newsfeed_page.dart';
import 'package:ti_yich_test/screens/profile_page.dart';

class FeedWidget extends StatelessWidget {
  final Map<String, dynamic> content;
  final DateTime date;
  final String type;
  final String uid;
  final DocumentReference? docRef;
  final List<dynamic> participants;
  FeedWidget(this.content, this.date, this.type, this.uid, this.docRef,
      this.participants,
      {Key? key})
      : super(key: key);
  String firstname = '';
  String name = '';
  String displayname = '';

  @override
  Widget build(BuildContext context) {
    DocumentReference<Map<String, dynamic>> _userRef =
        FirebaseFirestore.instance.collection('UserProfiles').doc(uid);
    return FutureBuilder<DocumentSnapshot>(
        future: _userRef.get(),
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            Map<String, dynamic> userdata =
                snapshot.data!.data() as Map<String, dynamic>;
            firstname = userdata['firstname'] ?? ' ';
            name = userdata['name'] ?? ' ';
            displayname = userdata['displayname'] ?? ' ';

            return ExpansionTile(
              leading: GestureDetector(
                child: CircleAvatar(
                  child: Text(displayname[0] + displayname[1]),
                  backgroundColor: PseudoAvatarColor(displayname),
                ),
                onTap: () {
                  if (uid == FirebaseAuth.instance.currentUser!.uid) {
                    Navigator.of(context).pop();
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => Home(
                              index: 1,
                            )));
                  } else {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) =>
                            PublicProfilePage(profileUID: uid)));
                  }
                },
              ),
              title: Text(feedTitle(displayname, type, content['title'])),
              subtitle: Row(children: [
                if (participants
                    .contains(FirebaseAuth.instance.currentUser!.uid)) ...[
                  OutlinedButton(
                    onPressed: () {
                      _removeParticipant(docRef);
                    },
                    child: Text('Se désinscrire'),
                    style: OutlinedButton.styleFrom(shape: StadiumBorder()),
                  )
                ] else ...[
                  OutlinedButton(
                    onPressed: () {
                      _addParticipant(docRef);
                    },
                    child: Text('Je participe'),
                    style: OutlinedButton.styleFrom(shape: StadiumBorder()),
                  )
                ],
                SizedBox(
                  width: 10,
                ),
                Participants(context),
              ]),
              children: [
                ListTile(
                  leading: Text('Créneau'),
                  title: Text(content['slot']),
                ),
                ListTile(
                  leading: Text('Détails'),
                  title: Text(content['details']),
                ),
              ],
            );
          }
          return Text("loading");
        });
  }

  void _addParticipant(DocumentReference? ref) async {
    if (ref != null) {
      await ref.set({
        'participants':
            FieldValue.arrayUnion([FirebaseAuth.instance.currentUser!.uid]),
      }, SetOptions(merge: true));
    }
  }

  void _removeParticipant(DocumentReference? ref) async {
    if (ref != null) {
      await ref.set({
        'participants':
            FieldValue.arrayRemove([FirebaseAuth.instance.currentUser!.uid]),
      }, SetOptions(merge: true));
    }
  }

  Widget Participants(BuildContext context) {
    if (participants.length == 0) {
      return Text('0 Participants');
    }
    if (participants.length == 1) {
      return TextButton(
        child: Text('1 Participant'),
        onPressed: () {
          Navigator.of(context).push(MaterialPageRoute(
              builder: (_) => ParticipanstPage(idlist: participants)));
        },
      );
    }
    return TextButton(
      child: Text(participants.length.toString() + ' Participants'),
      onPressed: () {
        Navigator.of(context).push(MaterialPageRoute(
            builder: (_) => ParticipanstPage(idlist: participants)));
      },
    );
  }
}
